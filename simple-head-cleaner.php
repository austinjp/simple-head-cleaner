<?php
/*
Plugin Name: Simple Head Cleaner
Plugin URI: https://bitbucket.org/austinjp/simple-head-cleaner
Description: Yet another plugin to tidy up the &laquo;head&raquo; stuff in Wordpress.
Version: 0.0.3
Author: Austin Plunkett
Author URI: https://bitbucket.org/austinjp
*/


function simple_head_cleaner() {
    // FIXME Make this all configurable via an admin page
    $actions = [
	[
	    'tag' => 'wp_head',
	    'functions' => [
		[ 'rsd_link', null ],                     // really simple discovery link
		[ 'wp_generator', null ],                 // Wordpress version
		// [ 'feed_links', 2 ],                   // RSS feed links
		[ 'feed_links_extra', 3],                 // removes all extra rss feed links
		[ 'index_rel_link', null],                // remove link to index page
		[ 'wlwmanifest_link', null],              // remove wlwmanifest.xml (needed to support windows live writer)
		[ 'start_post_rel_link', 10],             // remove random post link
		[ 'parent_post_rel_link', 10],            // remove parent post link
		[ 'adjacent_posts_rel_link', 10],         // remove the next and previous post links
		[ 'adjacent_posts_rel_link_wp_head', 10], // ?
		[ 'wp_shortlink_wp_head', 10],            // ?
		[ 'rest_output_link_wp_head', 10 ],       // remove api.w.org link
		[ 'wp_oembed_add_discovery_links', 10 ],  // remove oembed links
		[ 'wp_oembed_add_host_js', null ],        // more oembed
		[ 'print_emoji_detection_script', 7 ]     // remove emoji detection JavaScript
	    ],
	], [
	    'tag' => 'wp_print_styles',
	    'functions' => [
		[ 'print_emoji_styles', null ],
	    ]
	]
    ];
    
    foreach ($actions as $a) {
	$tag = $a['tag'];
	$funcs = $a['functions'];
	foreach ($funcs as $f) {
	    $func = $f[0];
	    $priority = $f[1];
	    remove_action($tag, $func, $priority);
	}
    }
}
add_action('after_setup_theme', 'simple_head_cleaner');
